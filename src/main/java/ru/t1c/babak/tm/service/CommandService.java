package ru.t1c.babak.tm.service;

import ru.t1c.babak.tm.api.repository.ICommandRepository;
import ru.t1c.babak.tm.api.service.ICommandService;
import ru.t1c.babak.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
